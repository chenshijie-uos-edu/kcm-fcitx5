# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the kcm_fcitx5 package.
#
# Translators:
# csslayer <wengxt@gmail.com>, 2017
msgid ""
msgstr ""
"Project-Id-Version: kcm_fcitx5\n"
"Report-Msgid-Bugs-To: fcitx-dev@googlegroups.com\n"
"POT-Creation-Date: 2017-12-22 00:01-0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: csslayer <wengxt@gmail.com>, 2017\n"
"Language-Team: Turkish (https://www.transifex.com/fcitx/teams/12005/tr/)\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: src/layoutselector.cpp:345
#, kde-format
msgid "%1 (%2)"
msgstr ""

#: src/model.cpp:150
#, kde-format
msgctxt "%1 is language name, %2 is country name"
msgid "%1 (%2)"
msgstr ""

#: src/optionwidget.cpp:354 src/optionwidget.cpp:357 src/optionwidget.cpp:363
#: src/optionwidget.cpp:366 src/optionwidget.cpp:369 src/optionwidget.cpp:372
#: src/optionwidget.cpp:375
#, kde-format
msgid "%1:"
msgstr ""

#: layout/main.cpp:39
#, kde-format
msgid "A general keyboard layout viewer"
msgstr ""

#: src/module.cpp:72
#, kde-format
msgid "Addon"
msgstr ""

#: src/layoutselector.cpp:339
#, kde-format
msgid "Any language"
msgstr ""

#: src/module.cpp:60
#, kde-format
msgid "Author"
msgstr ""

#. i18n: file: src/impage.ui:173
#. i18n: ectx: property (text), widget (QLabel, label)
#: rc.cpp:36
#, kde-format
msgid "Available Input Method:"
msgstr "Kullanılabilir Giriş Yöntemi:"

#. i18n: file: src/erroroverlay.ui:167
#. i18n: ectx: property (text), widget (QLabel, textLabel)
#: rc.cpp:51
#, kde-format
msgid "Cannot connect to Fcitx by DBus, is Fcitx running?"
msgstr "DBus tarafından Fcitx'e bağlanılamıyor, Fcitx çalışıyor mu?"

#: src/impage.cpp:295
#, kde-format
msgid "Change Input method to match layout selection."
msgstr ""

#: src/impage.cpp:344
#, kde-format
msgid "Change System layout to match input method selection."
msgstr ""

#: src/module.cpp:56
#, kde-format
msgid "Configure Fcitx 5"
msgstr ""

#: src/kcm_fcitx5.desktop.in:13
msgid "Configure Input Method"
msgstr ""

#: src/module.cpp:57
#, kde-format
msgid "Copyright 2017 Xuetian Weng"
msgstr ""

#. i18n: file: src/impage.ui:24
#. i18n: ectx: property (text), widget (QLabel, label_2)
#: rc.cpp:24
#, kde-format
msgid "Current Input Method:"
msgstr "Geçerli Giriş Yöntemi:"

#: src/impage.cpp:197
#, kde-format
msgid "Current group changed"
msgstr ""

#: src/layoutselector.cpp:134
#, kde-format
msgid "Default"
msgstr "Varsayılan"

#: src/module.cpp:55
#, kde-format
msgid "Fcitx 5 Configuration Module"
msgstr ""

#. i18n: file: src/layoutselector.ui:14
#. i18n: ectx: property (windowTitle), widget (QWidget, LayoutSelector)
#. i18n: file: src/listoptionwidget.ui:14
#. i18n: ectx: property (windowTitle), widget (QWidget, ListOptionWidget)
#. i18n: file: src/fontbutton.ui:20
#. i18n: ectx: property (windowTitle), widget (QWidget, FontButton)
#. i18n: file: src/erroroverlay.ui:132
#. i18n: ectx: property (windowTitle), widget (QWidget, ErrorOverlay)
#. i18n: file: src/addonselector.ui:14
#. i18n: ectx: property (windowTitle), widget (QWidget, AddonSelector)
#: rc.cpp:3 rc.cpp:15 rc.cpp:18 rc.cpp:48 rc.cpp:54
#, kde-format
msgid "Form"
msgstr "Form"

#: src/addonselector.cpp:178
#, kde-format
msgid "Frontend"
msgstr ""

#: src/module.cpp:79
#, kde-format
msgid "Global Config"
msgstr "Küresel Yapılandırma"

#: src/impage.cpp:438
#, kde-format
msgid "Group Name:"
msgstr ""

#. i18n: file: src/impage.ui:33
#. i18n: ectx: property (text), widget (QLabel, label_5)
#: rc.cpp:27
#, kde-format
msgid "Group:"
msgstr ""

#: src/module.cpp:67 src/addonselector.cpp:178 src/kcm_fcitx5.desktop.in:12
#, kde-format
msgid "Input Method"
msgstr "Giriş Yöntemi"

#: layout/main.cpp:43
#, kde-format
msgid "Keyboard <layout>"
msgstr "Klavye"

#: layout/main.cpp:71
#, kde-format
msgid "Keyboard Layout viewer"
msgstr ""

#: layout/main.cpp:42
#, kde-format
msgid "Keyboard layout <group> (0-3)"
msgstr "Klavye düzen (0-3)"

#: layout/main.cpp:45
#, kde-format
msgid "Keyboard layout <variant>"
msgstr "Klavye Düzeni"

#. i18n: file: src/impage.ui:77
#. i18n: ectx: property (toolTip), widget (QPushButton, defaultLayoutButton)
#: rc.cpp:30
#, kde-format
msgid "Keyboard layout to use when no input method active"
msgstr "Giriş yöntemi etkin değilken kullanılacak klavye düzeni"

#: layout/kbd-layout-viewer5.desktop.in:7
msgid "Keyboard layout viewer"
msgstr "Klavye Düzeni görüntüleyici"

#. i18n: file: src/layoutselector.ui:22
#. i18n: ectx: property (text), widget (QLabel, label_3)
#: rc.cpp:6
#, kde-format
msgid "Language:"
msgstr ""

#. i18n: file: src/layoutselector.ui:39
#. i18n: ectx: property (text), widget (QLabel, label)
#: rc.cpp:9
#, kde-format
msgid "Layout:"
msgstr ""

#: src/addonselector.cpp:179
#, kde-format
msgid "Loader"
msgstr ""

#: src/addonselector.cpp:179
#, kde-format
msgid "Module"
msgstr ""

#: src/model.cpp:113
#, kde-format
msgid "Multilingual"
msgstr "Çok dilli"

#: src/impage.cpp:438
#, kde-format
msgid "New Group"
msgstr ""

#: src/optionwidget.cpp:420 src/optionwidget.cpp:422
#, kde-format
msgid "No"
msgstr ""

#. i18n: file: src/impage.ui:212
#. i18n: ectx: property (text), widget (QCheckBox, onlyCurrentLanguageCheckBox)
#: rc.cpp:42
#, kde-format
msgid "Only &Show Current Language"
msgstr "Göster :&Sadece Geçerli Dili"

#: src/model.cpp:133
#, kde-format
msgid "Other"
msgstr "Diğer"

#. i18n: file: src/addonselector.ui:32
#. i18n: ectx: property (placeholderText), widget (QLineEdit, lineEdit)
#: rc.cpp:57
#, kde-format
msgid "Search Addons"
msgstr ""

#. i18n: file: src/impage.ui:180
#. i18n: ectx: property (placeholderText), widget (QLineEdit, filterTextEdit)
#: rc.cpp:39
#, kde-format
msgid "Search Input Method"
msgstr "Giriş Yöntemi Ara"

#. i18n: file: src/fontbutton.ui:51
#. i18n: ectx: property (text), widget (QPushButton, fontSelectButton)
#: rc.cpp:21
#, kde-format
msgid "Select &Font..."
msgstr "Seç &Yazı tipi..."

#: src/impage.cpp:282
#, kde-format
msgid "Select default layout"
msgstr ""

#. i18n: file: src/impage.ui:80
#. i18n: ectx: property (text), widget (QPushButton, defaultLayoutButton)
#: rc.cpp:33
#, kde-format
msgid "Select system keyboard layout"
msgstr ""

#. i18n: file: src/addonselector.ui:70
#. i18n: ectx: property (text), widget (QCheckBox, advancedCheckbox)
#: rc.cpp:60
#, kde-format
msgid "Show &Advanced options"
msgstr ""

#. i18n: file: src/impage.ui:269
#. i18n: ectx: property (text), widget (QLabel, infoLabel)
#: rc.cpp:45
#, kde-format
msgid ""
"The first input method will be inactive state. Usually you need to put "
"<b>Keyboard</b> or <b>Keyboard - <i>layout name</i></b> in the first place."
msgstr ""
"İlk giriş yöntemi aktif değildirr. Genellikle <b> Klavye </ b> veya <b> "
"Klavye - <i> düzen adı </ i> </ b> koymanız gerekir."

#: src/addonselector.cpp:180
#, kde-format
msgid "UI"
msgstr ""

#: src/model.cpp:111
#, kde-format
msgid "Unknown"
msgstr "Bilinmeyen"

#. i18n: file: src/layoutselector.ui:56
#. i18n: ectx: property (text), widget (QLabel, label_2)
#: rc.cpp:12
#, kde-format
msgid "Variant:"
msgstr ""

#: layout/kbd-layout-viewer5.desktop.in:8
msgid "View keyboard layout"
msgstr ""

#: src/module.cpp:60
#, kde-format
msgid "Xuetian Weng"
msgstr "Xuetian Weng"

#: src/optionwidget.cpp:420 src/optionwidget.cpp:422
#, kde-format
msgid "Yes"
msgstr ""

#: src/impage.cpp:345
#, kde-format
msgid ""
"Your currently configured input method does not match your layout, do you "
"want to change the layout setting?"
msgstr ""

#: src/impage.cpp:296
#, kde-format
msgid ""
"Your currently configured input method does not match your selected layout, "
"do you want to add the corresponding input method for the layout?"
msgstr ""

#: src/optionwidget.cpp:453
#, kde-format
msgid "[%1]"
msgstr ""

#: layout/main.cpp:42
#, kde-format
msgid "group"
msgstr ""

#: layout/main.cpp:43
#, kde-format
msgid "layout"
msgstr "düzeni"

#: layout/main.cpp:46
#, kde-format
msgid "variant"
msgstr "türü"
